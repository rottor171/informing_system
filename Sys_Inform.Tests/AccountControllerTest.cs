﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Sys_Inform.Controllers;
using System.Web.Mvc;

namespace Sys_Inform.Tests
{
    [TestClass]
    public class AccountControllerTest
    {
        private AccountController acc;
        private ViewResult result;

        [TestInitialize]
        public void SetupContext()
        {
            acc = new AccountController();
            result = acc.Register() as ViewResult;
        }

        [TestMethod]
        public void RegisterIsNotNull()
        {
            Assert.IsNotNull(result);
        }
    }
}
