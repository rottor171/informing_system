﻿using Sys_Inform.DataAccessLayer;
using Sys_Inform.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Sys_Inform.Controllers
{
    public class WebSitesController : Controller
    {
        static IRepository<WebSite> repo;
        static IRepository<U_S_Link> linkRepo;
        // GET: WebSites

        static WebSitesController()
        {
            repo = new DRepository<WebSite>();
            linkRepo = new DRepository<U_S_Link>();
        }

        public ActionResult AddSite()
        {
            return View();
        }

        // POST: WebSites
        [HttpPost]
        public ActionResult AddSite(AddSiteViewModel model)
        {
            if (ModelState.IsValid)
            {
                var website = new WebSite() { Name = model.Name, Link = model.Link };
                Load.Update_L(website, model.Time);
                return RedirectToAction("SiteList", "WebSites");
            }

            return View(model);
        }
        
        // GET: Notifications
        public ActionResult SiteList()
        {
            var model = new List<WebSite>();
            string id = Load.Id();
            var l1 = linkRepo.Get(uid => uid.U_Id == id);
            List<U_S_Link> l2 = l1.ToList();
            List<WebSite> list = new List<WebSite>();
            foreach (U_S_Link elem in l2)
            {
                var ll = Repository.IsAnyWebSite(sid => sid.Id == elem.Sid && elem.U_Id == id);
                if (ll != null) list.Add(ll);
            }
            foreach (var item in list)
            {
                model.Add(item);
            }
            return View(model);
        }

        //POST: Notifications
        [HttpPost]
        //[AllowAnonymous]
        //[ValidateAntiForgeryToken]
        public ActionResult SiteList(NotifListViewModel model)
        {
            if (ModelState.IsValid)
            {
                SiteList();
            }

            return View(model);
        }
    }
}