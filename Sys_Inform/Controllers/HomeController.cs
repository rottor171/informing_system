﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Sys_Inform.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Description:";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Feel free to contact us at:";

            return View();
        }
    }
}