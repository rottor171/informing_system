﻿using Sys_Inform.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Sys_Inform.Controllers
{
    public class NotificationsController : Controller
    {
        // GET: Notifications
        public ActionResult AddNotif()
        {
            return View();
        }

        //POST: Notifications
        [HttpPost]
        //[AllowAnonymous]
        //[ValidateAntiForgeryToken]
        public ActionResult AddNotif(AddNotifViewModel model)
        {
            if (ModelState.IsValid)
            {
                var notif = new Notification() { Name = model.Name, Time = model.Time, PrevDate = model.Date, Txt = model.Txt };
                Load.Update_N(notif);

                return RedirectToAction("NotifList", "Notifications");
            }

            return View(model);
        }

        // GET: Notifications
        public ActionResult NotifList()
        {
            var model = new List<Notification>();
            string id = Load.Id();
            var repo = new DRepository<Notification>();
            var list = repo.Get(x => x.U_Id == id);
            foreach (var item in list)
            {
                model.Add(item);
            }
            return View(model);
        }

        //POST: Notifications
        [HttpPost]
        //[AllowAnonymous]
        //[ValidateAntiForgeryToken]
        public ActionResult NotifList(NotifListViewModel model)
        {
            if (ModelState.IsValid)
            {
                NotifList();
            }

            return View(model);
        }

        public ActionResult Edit()
        {
            return View();
        }

        //POST: Notifications
        [HttpPost]
        //[AllowAnonymous]
        //[ValidateAntiForgeryToken]
        public ActionResult Edit(EditViewModel model)
        {
            if (ModelState.IsValid)
            {
                Load.Update_N(new Notification { Name = model.Name, PrevDate = model.PrevDate, Time = model.Time, Txt = model.Txt, Id = model.Id });
                return View("NotifList","Notification");
            }

            return View(model);
        }
    }
}