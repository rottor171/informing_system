﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Sys_Inform.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace Sys_Inform
{
    public static class Repository
    {
        public static ApplicationUser GetUser()
        {
            //var store = new UserStore<ApplicationUser>(new ApplicationDbContext());
            ApplicationUser user = new ApplicationUser();
            var id = HttpContext.Current.User.Identity.GetUserId();
            using (var store = new ApplicationDbContext())
            {
                user = store.Users.FirstOrDefault(s => s.Id == id);
            }
            return user;
        }
        public static int NewNotifId()
        {
            using(ApplicationDbContext context = new ApplicationDbContext()){
                if (IsAnyNotif()) return context.Notification.ToList().LastOrDefault().Id + 1;
                else return 0;
            }
        }
        public static int NewSiteId(string link)
        {
            ApplicationDbContext context = new ApplicationDbContext();
            var ws = IsAnyWebSite(s => s.Link == link);
            if(IsAnyWebSite() == false) return 0;
            if (ws != default(WebSite)) return ws.Id;
            else return context.WebSite.ToList().LastOrDefault().Id + 1;
        }
        public static int NewLinkId()
        {
            ApplicationDbContext context = new ApplicationDbContext();
            if (IsAnyLink()) return context.U_S_Link.ToList().LastOrDefault().Id + 1;
            else return 0;
        }
        public static void Add(Notification newnote)
        {
            using (ApplicationDbContext context = new ApplicationDbContext())
            {
                context.Notification.Add(newnote);
                context.SaveChanges();
            }//context.Dispose();
        }
        public static void Add(WebSite website)
        {
            ApplicationDbContext context = new ApplicationDbContext();
            context.WebSite.Add(website);
            context.SaveChanges();
            context.Dispose();
        }
        public static void Add(U_S_Link link)
        {
            ApplicationDbContext context = new ApplicationDbContext();
            context.U_S_Link.Add(link);
            context.SaveChanges();
            context.Dispose();
        }
        public static bool IsAnyWebSite()
        {
            ApplicationDbContext context = new ApplicationDbContext();
            string id = Load.Id();
            if (context.WebSite.FirstOrDefault() == null) return false;
            else return true;
        }
        public static bool IsAnyLink()
        {
            ApplicationDbContext context = new ApplicationDbContext();
            string id = Load.Id();
            if (context.U_S_Link.FirstOrDefault(uid => uid.U_Id == id) == null) return false;
            else return true;
        }
        public static bool IsAnyNotif()
        {
            ApplicationDbContext context = new ApplicationDbContext();
            string id = Load.Id();
            if (context.Notification.FirstOrDefault(uid => uid.U_Id == id) == null) return false;
            else return true;
        }
        public static WebSite IsAnyWebSite(Expression<Func<WebSite, bool>> p)
        {
            ApplicationDbContext context = new ApplicationDbContext();
            var x = context.WebSite.FirstOrDefault(p);
            if (x != null) return x;
            else return default(WebSite);
        }
        public static U_S_Link IsAnyLink(Expression<Func<U_S_Link, bool>> p)
        {
            ApplicationDbContext context = new ApplicationDbContext();
            var x = context.U_S_Link.FirstOrDefault(p);
            if (x != null) return x;
            else return default(U_S_Link);
        }
        public static Notification IsAnyNotif(Expression<Func<Notification, bool>> p)
        {
            ApplicationDbContext context = new ApplicationDbContext();
            var x = context.Notification.FirstOrDefault(p);
            if (x != null) return x;
            else return default(Notification);
        }
        public static List<WebSite> GetWebSites(Expression<Func<WebSite, bool>> p)
        {
            ApplicationDbContext context = new ApplicationDbContext();
            return (List<WebSite>)context.WebSite.Where(p);
            //return new List<U_S_Link>();
        }
        public static List<U_S_Link> GetLinks(Expression<Func<U_S_Link, bool>> p)
        {
            ApplicationDbContext context = new ApplicationDbContext();
            return context.U_S_Link.Where(p).ToList();
            //return new List<U_S_Link>();
        }
        public static void Remove(U_S_Link o)
        {
            ApplicationDbContext context = new ApplicationDbContext();
            context.U_S_Link.Remove(o);
            context.SaveChanges();
        }
        public static void Remove(Notification o)
        {
            ApplicationDbContext context = new ApplicationDbContext();
            context.Notification.Remove(o);
            context.SaveChanges();
        }
        public static void Remove(string lnk)
        {
            ApplicationDbContext context = new ApplicationDbContext();
            var s = IsAnyWebSite(sid => sid.Link == lnk);
            string id = Load.Id();
            var us = IsAnyLink(sid => sid.Sid == s.Id && sid.U_Id == id);
            Remove(us);
        }
        public static List<Notification> GetNotifs(Expression<Func<Notification, bool>> p)
        {
            ApplicationDbContext context = new ApplicationDbContext();
            return (List<Notification>)context.Notification.Where(p);
        }
        //        <script type = "text/javascript" >
        //        function goToPage()
        //        {
        //            var url = document.getElementById('id_Элемента');
        //            document.location.href = url.value;
        //        }
        //        </script>
    }
}