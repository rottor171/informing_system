﻿using Sys_Inform.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Dapper;
using System.Data.SqlClient;
using System.Reflection;
using System.Configuration;
using Sys_Inform.DataAccessLayer;

namespace Sys_Inform
{
    public class DRepository<T> : IRepository<T>
    {
        public static SqlConnection connection = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
        public int NewId(T t)
        {
            using (connection)
            {
                try
                {
                    if (t.GetType() == typeof(Notification))
                    {
                        string sqlQ = "SELECT * FROM Notifications";
                        return connection.Query(sqlQ).ToList().LastOrDefault().Id;
                    }
                    else if (t.GetType() == typeof(WebSite))
                    {
                        string sqlQ = "SELECT * FROM WebSites";
                        return connection.Query(sqlQ).ToList().LastOrDefault().Id;
                    }
                    else if (t.GetType() == typeof(U_S_Link))
                    {
                        string sqlQ = "SELECT * FROM U_S_Link";
                        return connection.Query(sqlQ).ToList().LastOrDefault().Id;
                    }
                }
                catch (NullReferenceException)
                { return 0; }
                return 0;
            }
        }
        public void Add(T t)
        {
            using (connection)
            {
                if (t.GetType() == typeof(Notification))
                {
                    string sqlQ = "SET IDENTITY_INSERT Notifications ON  INSERT INTO [dbo].[Notifications]([Id], [Name], [PrevDate], [Time], [Txt], [U_Id]) VALUES (@Id, @Name, @PrevDate, @Time, @Txt, @U_Id)  SET IDENTITY_INSERT Notifications OFF";
                    connection.Execute(sqlQ, new
                    {
                        Id = t.GetType().GetProperty("Id", BindingFlags.Public | BindingFlags.Instance).GetValue(t),
                        Name = t.GetType().GetProperty("Name", BindingFlags.Public | BindingFlags.Instance).GetValue(t),
                        PrevDate = t.GetType().GetProperty("PrevDate", BindingFlags.Public | BindingFlags.Instance).GetValue(t),
                        Time = t.GetType().GetProperty("Time", BindingFlags.Public | BindingFlags.Instance).GetValue(t),
                        Txt = t.GetType().GetProperty("Txt", BindingFlags.Public | BindingFlags.Instance).GetValue(t),
                        U_Id = t.GetType().GetProperty("U_Id", BindingFlags.Public | BindingFlags.Instance).GetValue(t)
                    });
                }
                if (t.GetType() == typeof(WebSite))
                {
                    string sqlQ = "SET IDENTITY_INSERT Notifications ON  INSERT INTO [dbo].[Notifications]([Id], [Link], [Txt], [Name], [PrevDate]) VALUES (@Id, @Link, @Txt, @Name, @PrevDate)  SET IDENTITY_INSERT Notifications OFF";
                    connection.Execute(sqlQ, new
                    {
                        Id = t.GetType().GetProperty("Id", BindingFlags.Public | BindingFlags.Instance).GetValue(t),
                        Link = t.GetType().GetProperty("Link", BindingFlags.Public | BindingFlags.Instance).GetValue(t),
                        Txt = t.GetType().GetProperty("Txt", BindingFlags.Public | BindingFlags.Instance).GetValue(t),
                        Name = t.GetType().GetProperty("Name", BindingFlags.Public | BindingFlags.Instance).GetValue(t),
                        PrevDate = t.GetType().GetProperty("PrevDate", BindingFlags.Public | BindingFlags.Instance).GetValue(t)
                    });
                }
                if (t.GetType() == typeof(U_S_Link))
                {
                    string sqlQ = "SET IDENTITY_INSERT Notifications ON  INSERT INTO [dbo].[Notifications]([Id], [U_Id], [Sid], [Time], [Seen], [LastDate]) VALUES (@Id, @U_Id, @Sid, @Time, @Seen, @LastDate)  SET IDENTITY_INSERT Notifications OFF";
                    connection.Execute(sqlQ, new
                    {
                        Id = t.GetType().GetProperty("Id", BindingFlags.Public | BindingFlags.Instance).GetValue(t),
                        U_Id = t.GetType().GetProperty("U_Id", BindingFlags.Public | BindingFlags.Instance).GetValue(t),
                        Sid = t.GetType().GetProperty("Sid", BindingFlags.Public | BindingFlags.Instance).GetValue(t),
                        Time = t.GetType().GetProperty("Time", BindingFlags.Public | BindingFlags.Instance).GetValue(t),
                        Seen = t.GetType().GetProperty("Seen", BindingFlags.Public | BindingFlags.Instance).GetValue(t),
                        LastDate = t.GetType().GetProperty("LastDate", BindingFlags.Public | BindingFlags.Instance).GetValue(t)
                    });
                }
            }
        }
        public  bool IsAny(T t)
        {
            using (connection)
            {
                if (t.GetType() == typeof(Notification))
                {
                    string sqlQ = "SELECT * FROM Notifications";
                    if (connection.Query(sqlQ).ToList() != null) return true;
                }
                else if (t.GetType() == typeof(WebSite))
                {
                    string sqlQ = "SELECT * FROM WebSites";
                    if (connection.Query(sqlQ).ToList() != null) return true;
                }
                else if (t.GetType() == typeof(U_S_Link))
                {
                    string sqlQ = "SELECT * FROM U_S_Link";
                    if (connection.Query(sqlQ).ToList() != null) return true;
                }
                return false;
            }
        }
        public  List<T> Get(Expression<Func<T, bool>> p)
        {
            using (connection)
            {
                // param.Name, left.Name, operation.NodeType, right.Value);
                // This code produces the following output:  
                // Decomposed expression: num => num LessThan 5
                if (typeof(T) == typeof(Notification))
                {
                    string sqlQ = "SELECT * FROM Notifications WHERE ";
                    ParameterExpression param = p.Parameters[0];
                    BinaryExpression operation = (BinaryExpression)p.Body;
                    MemberExpression left = (MemberExpression)operation.Left;
                    sqlQ += left.Member.Name;
                    //MemberExpression right = (MemberExpression)operation.Right;
                    var op = Expression.Lambda<Func<string>>(operation.Right);
                    Func<string> accessor = op.Compile();
                    var value = accessor();
                    if (operation.NodeType.ToString() == "LessThan") sqlQ += " <";
                    else if (operation.NodeType.ToString() == "GreaterThan") sqlQ += " >";
                    else if (operation.NodeType.ToString() == "LessThanOrEqual") sqlQ += " <=";
                    else if (operation.NodeType.ToString() == "GreaterThanOrEqual") sqlQ += " >=";
                    else if (operation.NodeType.ToString() == "Equal") sqlQ += " =";
                    else if (operation.NodeType.ToString() == "NotEqual") sqlQ += " !=";
                    sqlQ += " \'" + value + "\'";
                    return connection.Query<T>(sqlQ).ToList();
                }
                else if (typeof(T) == typeof(WebSite))
                {
                    string sqlQ = "SELECT * FROM WebSites WHERE ";
                    ParameterExpression param = p.Parameters[0];
                    BinaryExpression operation = (BinaryExpression)p.Body;
                    MemberExpression left = (MemberExpression)operation.Left;
                    sqlQ += left.Member.Name;
                    //MemberExpression right = (MemberExpression)operation.Right;
                    var op = Expression.Lambda<Func<string>>(operation.Right);
                    Func<string> accessor = op.Compile();
                    var value = accessor();
                    if (operation.NodeType.ToString() == "LessThan") sqlQ += " <";
                    else if (operation.NodeType.ToString() == "GreaterThan") sqlQ += " >";
                    else if (operation.NodeType.ToString() == "LessThanOrEqual") sqlQ += " <=";
                    else if (operation.NodeType.ToString() == "GreaterThanOrEqual") sqlQ += " >=";
                    else if (operation.NodeType.ToString() == "Equal") sqlQ += " =";
                    else if (operation.NodeType.ToString() == "NotEqual") sqlQ += " !=";
                    sqlQ += " \'" + value + "\'";
                    return connection.Query<T>(sqlQ).ToList();
                }
                else if (typeof(T) == typeof(U_S_Link))
                {
                    string sqlQ = "SELECT * FROM [dbo].[U_S_Link] WHERE ";
                    ParameterExpression param = p.Parameters[0];
                    BinaryExpression operation = (BinaryExpression)p.Body;
                    MemberExpression left = (MemberExpression)operation.Left;
                    sqlQ += left.Member.Name;
                    //MemberExpression right = (MemberExpression)operation.Right;
                    var op = Expression.Lambda<Func<string>>(operation.Right);
                    Func<string> accessor = op.Compile();
                    var value = accessor();
                    if (operation.NodeType.ToString() == "LessThan") sqlQ += " <";
                    else if (operation.NodeType.ToString() == "GreaterThan") sqlQ += " >";
                    else if (operation.NodeType.ToString() == "LessThanOrEqual") sqlQ += " <=";
                    else if (operation.NodeType.ToString() == "GreaterThanOrEqual") sqlQ += " >=";
                    else if (operation.NodeType.ToString() == "Equal") sqlQ += " =";
                    else if (operation.NodeType.ToString() == "NotEqual") sqlQ += " !=";
                    sqlQ += " \'" + value + "\'";
                    return connection.Query<T>(sqlQ).ToList();
                }
                else return new List<T>();
            }
        }
        public  void Remove(T t)
        {
            using (connection)
            {
                if (t.GetType() == typeof(Notification))
                {
                    string sqlQ = "DELETE FROM Notifications WHERE Id = @Id";
                    connection.Execute(sqlQ, new
                    {
                        Id = t.GetType().GetProperty("Id", BindingFlags.Instance).GetValue(t)
                    });
                }
                if (t.GetType() == typeof(WebSite))
                {
                    string sqlQ = "DELETE FROM WebSites WHERE Id = @Id";
                    connection.Execute(sqlQ, new
                    {
                        Id = t.GetType().GetProperty("Id", BindingFlags.Instance).GetValue(t)
                    });
                }
                if (t.GetType() == typeof(U_S_Link))
                {
                    string sqlQ = "DELETE FROM U_S_Links WHERE Id = @Id";
                    connection.Execute(sqlQ, new
                    {
                        Id = t.GetType().GetProperty("Id", BindingFlags.Instance).GetValue(t)
                    });
                }
            }
        }
        public void Update(T t)
        {
            using (connection)
            {
                if (t.GetType() == typeof(Notification))
                {
                    string sqlQ = "UPDATE [dbo].[Notifications] SET Name = @Name, PrevDate = @PrevDate,Time = @Time,Txt = @Txt WHERE Id = @Id";
                    connection.Execute(sqlQ, new
                    {
                        Id = t.GetType().GetProperty("Id", BindingFlags.Public | BindingFlags.Instance).GetValue(t),
                        Name = t.GetType().GetProperty("Name", BindingFlags.Public | BindingFlags.Instance).GetValue(t),
                        PrevDate = t.GetType().GetProperty("PrevDate", BindingFlags.Public | BindingFlags.Instance).GetValue(t),
                        Time = t.GetType().GetProperty("Time", BindingFlags.Public | BindingFlags.Instance).GetValue(t),
                        Txt = t.GetType().GetProperty("Txt", BindingFlags.Public | BindingFlags.Instance).GetValue(t)
                    });
                }
                if (t.GetType() == typeof(WebSite))
                {
                    string sqlQ = "UPDATE [dbo].[WebSites] SET Name = @Name, Txt = @Txt, PrevDate = @PrevDate WHERE Id = @Id";
                    connection.Execute(sqlQ, new
                    {
                        Id = t.GetType().GetProperty("Id", BindingFlags.Public | BindingFlags.Instance).GetValue(t),
                        Link = t.GetType().GetProperty("Link", BindingFlags.Public | BindingFlags.Instance).GetValue(t),
                        Txt = t.GetType().GetProperty("Txt", BindingFlags.Public | BindingFlags.Instance).GetValue(t),
                        Name = t.GetType().GetProperty("Name", BindingFlags.Public | BindingFlags.Instance).GetValue(t),
                        PrevDate = t.GetType().GetProperty("PrevDate", BindingFlags.Public | BindingFlags.Instance).GetValue(t)
                    });
                }
                if (t.GetType() == typeof(U_S_Link))
                {
                    string sqlQ = "UPDATE [dbo].[U_S_Links] SET Time = @Time, LastDate = @LastDate, Seen = @Seen WHERE Id = @Id";
                    connection.Execute(sqlQ, new
                    {
                        Id = t.GetType().GetProperty("Id", BindingFlags.Public | BindingFlags.Instance).GetValue(t),
                        U_Id = t.GetType().GetProperty("U_Id", BindingFlags.Public | BindingFlags.Instance).GetValue(t),
                        Sid = t.GetType().GetProperty("Sid", BindingFlags.Public | BindingFlags.Instance).GetValue(t),
                        Time = t.GetType().GetProperty("Time", BindingFlags.Public | BindingFlags.Instance).GetValue(t),
                        Seen = t.GetType().GetProperty("Seen", BindingFlags.Public | BindingFlags.Instance).GetValue(t),
                        LastDate = t.GetType().GetProperty("LastDate", BindingFlags.Public | BindingFlags.Instance).GetValue(t)
                    });
                }
            }
        }
    }
}