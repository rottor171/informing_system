﻿using Sys_Inform.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Sys_Inform.DataAccessLayer
{
    public interface IRepository<T>
    {
        int NewId(T t);
        List<T> Get(Expression<Func<T, bool>> p);
        void Remove(T t);
        void Update(T t);
        bool IsAny(T t);
        void Add(T t);
    }
}
