﻿using Autofac;
using Autofac.Integration.Mvc;
using Sys_Inform;
using Sys_Inform.DataAccessLayer;
using System.Web.Mvc;

namespace Sys_Inform
{
    public class AutofacConfig
    {
        public static void ConfigureContainer()
        {
            // получаем экземпляр контейнера
            var builder = new ContainerBuilder();

            // регистрируем контроллер в текущей сборке
            builder.RegisterControllers(typeof(MvcApplication).Assembly);

            // регистрируем споставление типов
            builder.RegisterGeneric(typeof(DRepository<>))
            .As(typeof(IRepository<>));
            // создаем новый контейнер с теми зависимостями, которые определены выше
            var container = builder.Build();

            // установка сопоставителя зависимостей
            DependencyResolver.SetResolver(new AutofacDependencyResolver(container));
        }
    }
}