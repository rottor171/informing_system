﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNet.Identity;
using Newtonsoft.Json;
using System.Threading.Tasks;
using Sys_Inform.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Web;
using System.Data.Entity;
using Sys_Inform.DataAccessLayer;
using Sys_Inform;

namespace Sys_Inform
{
    public static class Load
    {
        static IRepository<Notification> nrepo;
        static IRepository<WebSite> wrepo;
        static IRepository<U_S_Link> lrepo;
        static Load()
        {
            nrepo = new DRepository<Notification>();
            wrepo = new DRepository<WebSite>();
            lrepo = new DRepository<U_S_Link>();
        }
        public struct WSInfo
        {
            public string name;
            public string txt;
        }

        public static string Id() { return HttpContext.Current.User.Identity.GetUserId(); }

        public static List<WebSite> LoadList()//Загрузка списка сайтов
        {
            string id = Id();
            if (!Repository.IsAnyLink()) return new List<WebSite>();
            else
            {
                var l1 = Repository.GetLinks(uid => uid.U_Id == id);
                List<U_S_Link> l2 = l1.ToList();
                List<WebSite> list2 = new List<WebSite>();
                foreach (U_S_Link elem in l2)
                {
                    var ll = Repository.IsAnyWebSite(sid => sid.Id == elem.Sid);
                    var lll = Repository.IsAnyLink(sid => sid.Sid == ll.Id && sid.Id == elem.Sid);
                    if (ll != null && lll != null)
                    { 
                        list2.Add(ll);
                    }
                }
                return list2;
            }
        }

        public static List<Notification> LoadNotifList()//Загрузка списка уведомлений
        {
            string id = Id();
            if (!Repository.IsAnyNotif()) return new List<Notification>();
            else
            {
                List<Notification> l1 = nrepo.Get(uid => uid.U_Id == id).ToList();
                List<Notification> l2 = l1.Where(i => (DateTime.Now.Date - i.PrevDate.Date).TotalDays <= i.Time).ToList();
                var json = JsonConvert.SerializeObject(l1);
                return l1;
            }
        }

        public static List<WebSite> Update_L(WebSite website, int time)//Добавление нового сайта в список
        {
            string id = Id();
            WSInfo w = Parser.Parse(website.Link);
            var newsite = new WebSite
            {
                Id = Repository.NewSiteId(website.Link),
                Link = website.Link,
                Txt = w.txt,
                Name = website.Name,
                PrevDate = DateTime.Now
            };
            if (Repository.IsAnyWebSite(s => s.Id == website.Id) == default(WebSite)) Repository.Add(newsite);//если сайт с такой ссылкой не существует, добавляем новый
            else wrepo.Update(website); 
            var tusl = Repository.IsAnyLink(sid => sid.U_Id == id && sid.Sid == newsite.Id);
            if (tusl == null)
            {
                var newlink = new U_S_Link
                {
                    Id = Repository.NewLinkId(),
                    U_Id = Id(),
                    Sid = newsite.Id,
                    Time = time,
                    Seen = false,
                    LastDate = DateTime.Now
                };
                Repository.Add(newlink);
            }
            else lrepo.Update(new U_S_Link { Id = tusl.Id, Time = time, Seen = false});
            return LoadList();
        }

        public static List<Notification> Update_N(Notification notif)//Добавление нового уведомления в список
        {
            var newnote = new Notification
            {
                Id = Repository.NewNotifId(),
                Name = notif.Name,
                Txt = notif.Txt,
                PrevDate = notif.PrevDate,
                Time = notif.Time,
                U_Id = Id()
            };
            if (Repository.IsAnyNotif(x => x.Id == notif.Id) != null) { newnote.Id = notif.Id; nrepo.Update(newnote); }
            else nrepo.Add(newnote);
            return LoadNotifList();
        }

        public static string LoadPrev(string link)//загрузка превью сайта
        {
            if (Repository.IsAnyWebSite(sid => sid.Link == link) == default(WebSite)) { return "nothing"; }
            else
            {
                var us = Repository.IsAnyWebSite(sid => sid.Link == link);
                var ls = Repository.IsAnyLink(sid => sid.Sid == us.Id);
                ls.Seen = true;
                List<Object> li = new List<object>() { us.Link, us.Name, us.PrevDate, ls.Time };
                var json = JsonConvert.SerializeObject(li);
                return json;
            }
        }

        public static List<WebSite> DelSite(WebSite w)//удаление сайта
        {
            if (Repository.IsAnyWebSite(sid => sid.Link == w.Link) == default(WebSite)) { return new List<WebSite>(); }
            else
            {
                wrepo.Remove(new WebSite { Link = w.Link });
                return LoadList();
            }
        }

        public static List<Notification> DelNot(Notification n)//удаление уведомления
        {
            if (Repository.IsAnyNotif(sid => sid.Name == n.Name) == default(Notification)) { return new List<Notification>(); }//"nothing"; }
            else
            {
                nrepo.Remove(n);
                return LoadNotifList();
            }
        }

        public static string LoadNotPrev(string n)//загрузка превью уведомления
        {
            var us = Repository.IsAnyNotif(sid => sid.Name == n);
            if (Repository.IsAnyNotif(sid => sid.Name == n) == default(Notification)) { return "nothing"; }
            else
            {
                List<Object> li = new List<object>() { us.Name, us.Txt, us.PrevDate, us.Time };
                var json = JsonConvert.SerializeObject(li);
                return json;
            }
        }

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        public static List<WebSite> RefreshT(int tim)//Обновление списка
        {
            string id = Id();
            if (Repository.IsAnyLink(uid => uid.U_Id == id) == default(U_S_Link)) return new List<WebSite>();
            else
            {
                List<U_S_Link> links = Repository.GetLinks(uid => uid.U_Id == id).ToList();
                List<WebSite> list2 = new List<WebSite>();
                List<WebSite> l3 = new List<WebSite>();
                foreach (U_S_Link elem in links)
                {
                    list2.Add(Repository.IsAnyWebSite(sid => sid.Id == elem.Sid && elem.U_Id == id));
                }
                foreach (WebSite item in list2)
                {
                    var ll = Repository.IsAnyLink(sid => sid.Sid == item.Id && sid.Time == tim);
                    if (ll == null) { }
                    else
                    {
                        if ((item.PrevDate - ll.LastDate).TotalMinutes > ll.Time)
                        {
                            var ii = Parser.Parse(item.Link);
                            if (item.Txt == ii.txt) { }
                            else
                            {
                                item.PrevDate = DateTime.Now;
                                item.Txt = ii.txt;
                                ll.Seen = false;
                                ll.LastDate = DateTime.Now;
                                wrepo.Update(item);
                                lrepo.Update(ll);
                            }
                        }
                    }
                }
                return LoadList();
            }
        }

        public static List<WebSite> Refresh()//Обновление списка
        {
            string id = Id();
            if (Repository.IsAnyLink(uid => uid.U_Id == id) == null) { return new List<WebSite>(); }//"nothing"; }
            else
            {
                List<U_S_Link> links = Repository.GetLinks(uid => uid.U_Id == id).ToList();
                List<WebSite> list2 = new List<WebSite>();
                List<WebSite> l3 = new List<WebSite>();
                foreach (U_S_Link elem in links)
                {
                    var ll = Repository.IsAnyWebSite(sid => sid.Id == elem.Sid);
                    if (ll != null) list2.Add(ll);
                }
                foreach (WebSite item in list2)
                {
                    var ll = Repository.IsAnyLink(sid => sid.Sid == item.Id && sid.U_Id == id);
                    if (ll == null) { }
                    else
                    {
                        if (DateTime.Now.Subtract(item.PrevDate).TotalMinutes > ll.Time)
                        {
                            var ii = Parser.Parse(item.Link);
                            if (item.Txt != ii.txt)
                            {
                                item.PrevDate = DateTime.Now;
                                item.Txt = ii.txt;
                                ll.Seen = false;
                                ll.LastDate = DateTime.Now;
                                wrepo.Update(item);
                                lrepo.Update(ll);
                            }
                        }
                    }
                }
                return LoadList();
            }
        }
    }
}
