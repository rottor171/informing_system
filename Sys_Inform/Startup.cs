﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Sys_Inform.Startup))]
namespace Sys_Inform
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
