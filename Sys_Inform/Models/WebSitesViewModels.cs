﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Sys_Inform.Models
{
    public class AddSiteViewModel
    {
        [Required]
        [Display(Name = "Link")]
        public string Link { get; set; }
        
        [Display(Name = "Name")]
        public string Name { get; set; }

        [Display(Name = "How often to check")]
        public int Time { get; set; }
    }

    public class SiteListViewModel
    {
        [Display(Name = "Link")]
        public string Link { get; set; }

        [Display(Name = "Name")]
        public string Name { get; set; }

        [Display(Name = "How often to check")]
        public int Time { get; set; }
    }
}