﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Sys_Inform.Models
{
    public class AddNotifViewModel
    {
        [Required]
        [Display(Name = "Name")]
        public string Name { get; set; }
        [Required]
        [Display(Name = "Text")]
        public string Txt { get; set; }
        [Required]
        [Display(Name = "Period of remindings")]
        public int Time { get; set; }
        [Required]
        [Display(Name = "First date")]
        public DateTime Date { get; set; }
    }

    public class NotifListViewModel
    {
        public List<Notification> Notifs { get; set; }

        [Display(Name = "Name")]
        public string Name { get; set; }
     
        [Display(Name = "Text")]
        public string Txt { get; set; }
     
        [Display(Name = "Period of remindings")]
        public int Time { get; set; }
     
        [Display(Name = "First date")]
        public DateTime PrevDate { get; set; }
    }

    public class EditViewModel
    {
        public int Id { get; set; }

        [Display(Name = "Name")]
        public string Name { get; set; }

        [Display(Name = "Text")]
        public string Txt { get; set; }

        [Display(Name = "Period of remindings")]
        public int Time { get; set; }

        [Display(Name = "First date")]
        public DateTime PrevDate { get; set; }
    }
}