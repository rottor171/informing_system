﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Collections.Generic;
using System;

namespace Sys_Inform.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }
    }//System.Web.HttpContext.Current.User.Identity.GetUserId();

    public class U_S_Link
    {
        public int Id { get; set; }
        public int? Sid { get; set; }
        public int Time { get; set; }
        public DateTime LastDate { get; set; }
        public bool? Seen { get; set; }
        public WebSite S { get; set; }
        public string U_Id { get; set; }
    }

    public class Notification
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Txt { get; set; }
        public DateTime PrevDate { get; set; }
        public int Time { get; set; }
        public string U_Id { get; set; }
    }

    public class WebSite
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Link { get; set; }
        public string Txt { get; set; }
        public DateTime PrevDate { get; set; }
    } 

    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public DbSet<Notification> Notification { get; set; }
        public DbSet<U_S_Link> U_S_Link { get; set; }
        public DbSet<WebSite> WebSite { get; set; }

        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
    }
}