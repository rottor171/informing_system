namespace Sys_Inform.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DataMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Notifications",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Txt = c.String(),
                        PrevDate = c.DateTime(nullable: false),
                        Time = c.Int(nullable: false),
                        Uid = c.Int(),
                        U_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.U_Id)
                .Index(t => t.U_Id);
            
            CreateTable(
                "dbo.U_S_Link",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Uid = c.Int(),
                        Sid = c.Int(),
                        Time = c.Int(nullable: false),
                        LastDate = c.DateTime(nullable: false),
                        Seen = c.Boolean(),
                        U_Id = c.String(maxLength: 128),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.WebSites", t => t.Sid)
                .ForeignKey("dbo.AspNetUsers", t => t.U_Id)
                .Index(t => t.Sid)
                .Index(t => t.U_Id);
            
            CreateTable(
                "dbo.WebSites",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Link = c.String(),
                        Txt = c.String(),
                        PrevDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.U_S_Link", "U_Id", "dbo.AspNetUsers");
            DropForeignKey("dbo.U_S_Link", "Sid", "dbo.WebSites");
            DropForeignKey("dbo.Notifications", "U_Id", "dbo.AspNetUsers");
            DropIndex("dbo.U_S_Link", new[] { "U_Id" });
            DropIndex("dbo.U_S_Link", new[] { "Sid" });
            DropIndex("dbo.Notifications", new[] { "U_Id" });
            DropTable("dbo.WebSites");
            DropTable("dbo.U_S_Link");
            DropTable("dbo.Notifications");
        }
    }
}
