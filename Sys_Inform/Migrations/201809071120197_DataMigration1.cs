namespace Sys_Inform.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DataMigration1 : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.Notifications", "Uid");
            DropColumn("dbo.U_S_Link", "Uid");
        }
        
        public override void Down()
        {
            AddColumn("dbo.U_S_Link", "Uid", c => c.Int());
            AddColumn("dbo.Notifications", "Uid", c => c.Int());
        }
    }
}
